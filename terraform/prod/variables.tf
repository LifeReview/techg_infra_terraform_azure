variable "subscription_id" {  
  description = "The Azure subscription id"
  default ="4a109e3f-8370-4183-ac8a-14d41ce7cb60"
}

variable "location" {
  description = "The location where the resources will be provisioned. This needs to be the same as where the image exists."
  default = "South East Asia"
}

variable "vm_resource_group" {
  description = "Name of the resource group in which to create the agents VMs."
  default = "rg_techg"
}

variable "image_name" {
  description = "The name of the image to use for the VM."
  default = "vsts-agent-ubuntu-1"
}

variable "image_resource_group" {
  description = "The name of the resource group where the image is located."
  default = "vsts-agent-images"
}

variable "vm_name" {
  description = "Virtual machine name. (a dash with the number of the VM will be appended to the name.  ex: agent-0, agent-1, etc.)"
  default = "vm-vsts"
}

variable "build_vm_size" {
  description = "Virtual Machine size"
  default     = "Standard_B2s"
}

variable "deploy_vm_size" {
  description = "Virtual Machine size"
  default     = "Standard_B2s"
}

variable "vm_instance_count" {
  description = "Virtual Machine instance count."
  default = "1"
}

variable "vm_admin" {
  description = "The username associated with the local administrator account on the VM (defaults to admin)."
  default = "vsts"
}

variable "vsts_account_name" {
  description = "The VSTS account name."
  default = "tuanmoc0606@gmail.com"
}

variable "vsts_pat" {
  description = "The VSTS personal access token."
  default ="pzw2j7izsuthaquv7xox7uvssqlv2o7dkecmal3p4bxamgjnwehq"
}

variable "vsts_agent_install_folder" {
  description = "The folder in which the agent was install (defaults to '/a1')."
  default = "/a1"
}

variable "vsts_agent_pool" {
  description = "The VSTS agent pool name (defaults to 'default')."
  default = "default"
}