# Configure the Microsoft Azure Provider
provider "azurerm" {
  version         = "~> 2"
  features { }
  subscription_id = var.subscription_id
}

# data "azurerm_image" "search" {
#   name                = var.image_name
#   resource_group_name = var.image_resource_group
# }

# output "image_id" {
#   value = data.azurerm_image.search.id
# }

resource "azurerm_resource_group" "main" {
  name     = var.vm_resource_group
  location = var.location
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.vm_name}-vnet"
  address_space       = ["172.16.0.0/16"]
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  virtual_network_name = azurerm_virtual_network.main.name
  resource_group_name  = azurerm_resource_group.main.name
  address_prefix       = "172.16.1.0/24"
}

resource "azurerm_public_ip" "techg_public_ip" {
  name                         = "${var.vm_name}-pip"
  location                     = azurerm_resource_group.main.location
  resource_group_name          = azurerm_resource_group.main.name
  allocation_method            = "Static"  
}

resource "azurerm_network_security_group" "main" {
  name                = "${var.vm_name}-nsg"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "allow_SSH"
    description                = "Allow SSH access"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_HTTP"
    description                = "Allow SSH access"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_HTTPS"
    description                = "Allow SSH access"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_TraefikAdmin"
    description                = "Allow SSH access"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_prometheus_public"
    description                = "Allow SSH access"
    priority                   = 104
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "9090"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_grafana_public"
    description                = "Allow SSH access"
    priority                   = 105
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "9091"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  
  security_rule {
    name                       = "docker_swarm_cluster_management_communication"
    description                = "Docker swarm cluster management communications"
    priority                   = 106
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "2377"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "docker_swarm_communication_among_nodes_tcp"
    description                = "Docker swarm communication among nodes tcp"
    priority                   = 107
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "7946"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "docker_swarm_communication_among_nodes_udp"
    description                = "Docker swarm communication among nodes udp"
    priority                   = 108
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "7946"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "docker_swarm_overlay_network_traffic_tcp"
    description                = "Docker swarm overlay network traffic tcp"
    priority                   = 109
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "4789"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "docker_swarm_overlay_network_traffic_udp"
    description                = "Docker swarm overlay network traffic udp"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "4789"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_all_tcp"
    description                = "Allow all tcp to test"
    priority                   = 111
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow_all_udp"
    description                = "Allow all udp to test"
    priority                   = 112
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# ------------------------------------------- VM 1 for build server -----------------------------------------------------------

resource "azurerm_network_interface" "main" {
  name                      = "${var.vm_name}-nic"
  location                  = azurerm_resource_group.main.location
  resource_group_name       = azurerm_resource_group.main.name
  # network_security_group_id = azurerm_network_security_group.main.id  

  ip_configuration {
    name                          = "primary"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Static"
    private_ip_address            = "172.16.1.11"
    public_ip_address_id          = azurerm_public_ip.techg_public_ip.id
  }
}

resource "azurerm_virtual_machine" "vm" {
  name                             = "${var.vm_name}"
  location                         = azurerm_resource_group.main.location
  resource_group_name              = azurerm_resource_group.main.name
  network_interface_ids            = [element(azurerm_network_interface.main.*.id, count.index)]
  vm_size                          = var.build_vm_size
  delete_os_disk_on_termination    = false
  delete_data_disks_on_termination = false
  count                            = var.vm_instance_count

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.vm_name}-count.index-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "StandardSSD_LRS"
    disk_size_gb      = "60"
  }

  # storage_data_disk {
  #   name              = var.vm_name}-${count.index}-data"
  #   managed_disk_type = "StandardSSD_LRS"
  #   create_option     = "Empty"
  #   lun               = 0
  #   disk_size_gb      = "30"
  # }

  os_profile {
    computer_name  = var.vm_name
    admin_username = var.vm_admin
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      key_data  = file("~/.ssh/id_rsa.pub")
      path      = "/home/${var.vm_admin}/.ssh/authorized_keys"
    }
  }
}

# data "template_file" "agent_command" {
#   template = "$${chdir} && $${config} && $${install_svc} && $${start_svc}"
#   vars = {
#     chdir = "cd ${var.vsts_agent_install_folder}"
#     config = "sudo ./bin/Agent.Listener configure --unattended --url 'https://${var.vsts_account_name}@dev.azure.com/tuanmoc0606' --auth pat --token ${var.vsts_pat} --pool ${var.vsts_agent_pool} --agent $(hostname) --acceptTeeEula"
#     install_svc = "sudo ./svc.sh install"
#     start_svc = "sudo ./svc.sh start"
#   }
# }

# ------------------------------------------- VM 2 for deploy server -----------------------------------------------------------

# resource "azurerm_public_ip" "deploy1_ip" {
#   name                         = "deploy1_ip"
#   location                     = azurerm_resource_group.main.location
#   resource_group_name          = azurerm_resource_group.main.name
#   public_ip_address_allocation = "static"
#   # count                        = var.vm_instance_count}"
# }

# resource "azurerm_network_interface" "deploy1_nic" {
#   name                      = "deploy1-nic"
#   location                  = azurerm_resource_group.main.location
#   resource_group_name       = azurerm_resource_group.main.name
#   network_security_group_id = azurerm_network_security_group.main.id
#   # count                     = var.vm_instance_count}"

#   ip_configuration {
#     name                          = "primary"
#     subnet_id                     = azurerm_subnet.internal.id
#     private_ip_address_allocation = "Static"
#     private_ip_address            = "172.16.1.12"
#     public_ip_address_id          = azurerm_public_ip.deploy1_ip.id
#   }
# }

# resource "azurerm_virtual_machine" "deploy1" {
#   name                             = "deploy1"
#   location                         = azurerm_resource_group.main.location
#   resource_group_name              = azurerm_resource_group.main.name
#   network_interface_ids            = [azurerm_network_interface.deploy1_nic.id]
#   vm_size                          = "Standard_B2s"
#   delete_os_disk_on_termination    = false
#   delete_data_disks_on_termination = false
#   # count                            = var.vm_instance_count}"

#   storage_image_reference {
#     id = data.azurerm_image.search.id
#   }

#   storage_os_disk {
#     name              = "deploy1-osdisk"
#     caching           = "ReadWrite"
#     create_option     = "FromImage"
#     managed_disk_type = "StandardSSD_LRS"
#     disk_size_gb      = "60"
#   }

#   storage_data_disk {
#     name              = "deploy1-data"
#     managed_disk_type = "StandardSSD_LRS"
#     create_option     = "Empty"
#     lun               = 0
#     disk_size_gb      = "30"
#   }

#   os_profile {
#     computer_name  = "deploy1-ubuntu"
#     admin_username = var.vm_admin
#   }

#   os_profile_linux_config {
#     disable_password_authentication = true
#     ssh_keys {
#       key_data  = file("~/.ssh/id_rsa.pub")
#       path      = "/home/${var.vm_admin}/.ssh/authorized_keys"
#     }
#   }
# }

# ------------------------------------------- VM 3 for testing -----------------------------------------------------------

# resource "azurerm_public_ip" "test1_ip" {
#   name                         = "test1_ip"
#   location                     = azurerm_resource_group.main.location}"
#   resource_group_name          = azurerm_resource_group.main.name}"
#   public_ip_address_allocation = "dynamic"
#   # count                        = var.vm_instance_count}"
# }

# resource "azurerm_network_interface" "test1_nic" {
#   name                      = "test1-nic"
#   location                  = azurerm_resource_group.main.location}"
#   resource_group_name       = azurerm_resource_group.main.name}"
#   network_security_group_id = azurerm_network_security_group.main.id}"
#   # count                     = var.vm_instance_count}"

#   ip_configuration {
#     name                          = "primary"
#     subnet_id                     = azurerm_subnet.internal.id}"
#     private_ip_address_allocation = "Static"
#     private_ip_address            = "172.16.1.13"
#     public_ip_address_id          = azurerm_public_ip.test1_ip.id}"
#   }
# }

# resource "azurerm_virtual_machine" "test1" {
#   name                             = "test1"
#   location                         = azurerm_resource_group.main.location}"
#   resource_group_name              = azurerm_resource_group.main.name}"
#   network_interface_ids            = [azurerm_network_interface.test1_nic.id}"]
#   vm_size                          = "Standard_B2s"
#   delete_os_disk_on_termination    = true
#   delete_data_disks_on_termination = true
#   # count                            = var.vm_instance_count}"

#   storage_image_reference {
#     id = data.azurerm_image.search.id}"
#   }

#   storage_os_disk {
#     name              = "test1-osdisk"
#     caching           = "ReadWrite"
#     create_option     = "FromImage"
#     managed_disk_type = "StandardSSD_LRS"
#     disk_size_gb      = "30"
#   }

#   # storage_data_disk {
#   #   name              = "deploy1-data"
#   #   managed_disk_type = "StandardSSD_LRS"
#   #   create_option     = "Empty"
#   #   lun               = 0
#   #   disk_size_gb      = "30"
#   # }

#   os_profile {
#     computer_name  = "test1-ubuntu"
#     admin_username = var.vm_admin}"
#   }

#   os_profile_linux_config {
#     disable_password_authentication = true
#     ssh_keys {
#       key_data  = file("~/.ssh/id_rsa.pub")}"
#       path      = "/home/${var.vm_admin}/.ssh/authorized_keys"
#     }
#   }
# }

# resource "azurerm_virtual_machine_extension" "agent_script" {
#   name                 = "agent_script"
#   location             = azurerm_resource_group.main.location}"
#   resource_group_name  = azurerm_resource_group.main.name}"
#   virtual_machine_name = azurerm_virtual_machine.test1.name}"
#   publisher            = "Microsoft.Azure.Extensions"
#   type                 = "CustomScript"
#   type_handler_version = "2.0"
#   count                = var.vm_instance_count}"

#   settings = <<SETTINGS
#     {
#       "commandToExecute": data.template_file.test_command.rendered}"
#     }
#   SETTINGS
# }

# data "template_file" "test_command" {
#   template = "$${chdir} && $${config} && $${install_svc} && $${start_svc}"
#   vars = {
#     chdir = "cd ${var.vsts_agent_install_folder}"
#     config = "sudo ./bin/Agent.Listener configure --unattended --url https://${var.vsts_account_name}@dev.azure.com/tuanmoc0606 --auth pat --token ${var.vsts_pat} --pool ${var.vsts_agent_pool} --agent $(hostname) --acceptTeeEula"
#     install_svc = "sudo ./svc.sh install"
#     start_svc = "sudo ./svc.sh start"
#   }
# }
# ------------------------------------------ Storage account for FE CDN --------------------------------------------- #

resource "azurerm_storage_account" "funniq_fe_prod" {
  name                = "safunniqfeprod"
  resource_group_name = azurerm_resource_group.main.name
  account_kind        = "StorageV2"
  location                 = azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "production"
  }
}

# ---------------------------------------- File Share --------------------------------------------------------- #

resource "azurerm_storage_share" "fs_funniq_prod" {
  name                 = "fsfunniqprod"
  storage_account_name = azurerm_storage_account.funniq_fe_prod.name
  quota                = 30

  # acl {
  #   id = "MTIzNDU2Nzg5MDEyMzQ1Njc4OTAxMjM0NTY3ODkwMTI"

  #   access_policy {
  #     permissions = "rwdl"
  #     start       = "2019-07-02T09:38:21.0000000Z"
  #     expiry      = "2019-07-02T10:38:21.0000000Z"
  #   }
  # }
}

# --------------------------------------- AZure CDN and Endpoints ---------------------------------------------- #

resource "azurerm_cdn_profile" "funniq_cdn" {
  name                = "funniq-cdn"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  sku                 = "Standard_Akamai"
}

resource "azurerm_cdn_endpoint" "funniq_fe_prod" {
  name                = "funniqfeprod"
  profile_name        = azurerm_cdn_profile.funniq_cdn.name
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  is_compression_enabled = true
  content_types_to_compress = [ "font/opentype",
                                "font/eot",
                                "font/ttf",
                                "font/otf", 
                                "font/woff", 
                                "font/woff2",
                                "text/javascript", 
                                "text/css", 
                                "text/plain", 
                                "text/html",
                                "text/js",
                                "application/javascript"]

  origin {
    name      = "funniqfeprod"
    host_name = "safunniqfeprod.z23.web.core.windows.net"    
  }

  origin_host_header = "safunniqfeprod.z23.web.core.windows.net"    
}

# ----------------------------------------------------------------- Azure Container Registry ---------------------------------------------------------- #

resource "azurerm_container_registry" "acr" {
  name                     = "TechGContainerRegistry"
  resource_group_name      = azurerm_resource_group.main.name
  location                 = azurerm_resource_group.main.location
  sku                      = "basic"
  admin_enabled            = true  
}

# ----------------------------------------------------------------- References ------------------------------------------------------------------------ #
# resource "azurerm_virtual_machine_extension" "agent_script" {
#   name                 = "agent_script"
#   location             = azurerm_resource_group.main.location}"
#   resource_group_name  = azurerm_resource_group.main.name}"
#   virtual_machine_name = element(azurerm_virtual_machine.vm.*.name, count.index)}"
#   publisher            = "Microsoft.Azure.Extensions"
#   type                 = "CustomScript"
#   type_handler_version = "2.0"
#   count                = var.vm_instance_count}"

#   settings = <<SETTINGS
#     {
#         "commandToExecute": data.template_file.agent_command.rendered}"
#     }
#   SETTINGS
# }